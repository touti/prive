# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

- !20: les valeurs d'environnement explicitement vidées en ajax passent par `var_nullify` pour en être totalement expurgées 
- !16: Suppréssion de la constante _DIR_RESTREINT_ABS
- !10: Permettre de trier les visiteurs 'nouveau' par date d'inscription-relance
- #3: Ajouter l'heure de publication à côté de la date
- !1: Utiliser des variables CSS dans l’espace privé pour éviter la compilation des fichiers CSS
- Composerisation

### Changed

- spip/medias#4958 Utilisation de `image_extensions_logos()` à la place de `$GLOBALS['formats_logos']`

### Fixed

- !13 Pouvoir modifier logo principal quand il y a un logo de survol
- spip/spip#3928 Les emails des auteurs sont masqués par défaut
